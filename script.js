// console.log("Testing s25 d1")

// [SECTION] JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- A common use for JSON is to read data from a web server, and display the data in a web page
	- Features of JSON:
		- It is a lightweight data-interchange format
		- It is easy to read and write
		- It is easy for machines to parse and generate
*/

// JSON Objects
//  - JSON also use the "key/value pairs"
//  - "Key/property" names require to be enclosed with double quotes.

/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "value"
	}
*/


// Exampole of JSON Object
/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"contry" : "Philippines"
}
*/

// Arrays of JSON Object

/*
"cities" : [
	{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"

	},
	{
		"city" "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"

	}

]
*/


// Array of Objects
let batchesArr = [
	{
		batchName: "Batch 218",
		schedule: "Part Time"
	},
	{
		batchName: "Batch 219",
		schedule: "Full Time"
	}
]

console.log(batchesArr);

// The "stringify" method is used to convert JavaScript objects into a string

console.log("Result of stringify method: ")
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines",
	}
})

console.log(data);

// let firstName = prompt("Enter your first name: ");
// let lastName = prompt("Enter your last name: ");
// let email = prompt("Enter your email: ");
// let password = prompt("Enter your email: ");

// let otherData = JSON.stringify({
// 	firstNam : firstName,
// 	lastName : lastName,
// 	email : email,
// 	password : password,
// });

// console.log(otherData);

// [SECTION] Converting Stringified JSON into Javascript Objects

let batchesJSON = `[
	{
		"batchName" : "Batch 218",
		"schedule" : "Full time"
	},
	{
		"batchName" : "Batch 218",
		"schedule" : "Full Time"
	}

]`


// console.log(JSON.parse(batchesJSON));

let parseBatches = JSON.parse(batchesJSON);

console.log(parseBatches);

console.log(parseBatches[0].batchName);

let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`;

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));